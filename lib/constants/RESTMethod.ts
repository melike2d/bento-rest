
export enum RESTMethod {
	GET = 'get',
  POST = 'post',
  USE = 'use',
	PUT = 'put',
	PATCH = 'patch',
  DELETE = 'delete'
}
