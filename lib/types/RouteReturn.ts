'use strict';

export type RouteReturn = string | number | { message?: string, status: number, [key: string]: any } | void;
