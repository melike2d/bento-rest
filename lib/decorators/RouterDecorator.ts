
import { applyDecorator, REST_COMPONENT_SYMBOL } from "./";
import { RESTMethod } from "../constants";

export function Router(name: string = "/"): ClassDecorator {
  return function(target: any) {
    target[REST_COMPONENT_SYMBOL].name = name;
  };
}

export function Use(path?: string): MethodDecorator {
  return function(
    target: any,
    propertyKey: string | symbol,
    descriptor: TypedPropertyDescriptor<any>
  ) {
    applyDecorator(
      RESTMethod.USE,
      path || "",
      target,
      propertyKey,
      descriptor,
      []
    );
  };
}
