import { RequestHandler } from "express";
import { RESTMethod } from "../constants";
import { applyDecorator, REST_COMPONENT_SYMBOL } from "./";

export type REST_ROUTE = {
  method: RESTMethod;
  path: string;
  fn: RequestHandler;
  handlers: RequestHandler[];
};

export interface REST_COMPONENT {
  name: string;
  routes: REST_ROUTE[];
}

export function getRestComponent(target: any): REST_COMPONENT | undefined {
  if (
    target.constructor == null ||
    !target.constructor[REST_COMPONENT_SYMBOL].name
  )
    return;
  if (!target.constructor[REST_COMPONENT_SYMBOL]) return;

  return target.constructor[REST_COMPONENT_SYMBOL];
}

/**
 * Creates a new REST Route.
 * @param {RESTMethod} method - The REST Method to assign.
 * @param {string} path - endpoint path
 * @param {RequestHandler[]} handlers - request handlers to assign.
 */
export function Route(
  method: RESTMethod,
  path: string,
  ...handlers: RequestHandler[]
): MethodDecorator {
  return function(
    target: any,
    propertyKey: string | symbol,
    descriptor: TypedPropertyDescriptor<any>
  ) {
    applyDecorator(method, path, target, propertyKey, descriptor, handlers);
  };
}

/**
 * Creates a new GET endpoint on the router.
 * @param {string} path - endpoint path.
 * @param {RequestHandler[]} handlers - request handlers to use.
 */
export function Get(
  path: string = "/",
  ...handlers: RequestHandler[]
): MethodDecorator {
  return function(
    target: any,
    propertyKey: string | symbol,
    descriptor: TypedPropertyDescriptor<any>
  ) {
    applyDecorator(
      RESTMethod.GET,
      path,
      target,
      propertyKey,
      descriptor,
      handlers
    );
  };
}

/**
 * Creates a new POST endpoint on the router.
 * @param {string} path - endpoint path.
 * @param {RequestHandler[]} handlers - request handlers to use.
 */
export function Post(
  path: string = "/",
  ...handlers: RequestHandler[]
): MethodDecorator {
  return function(
    target: any,
    propertyKey: string | symbol,
    descriptor: TypedPropertyDescriptor<any>
  ) {
    applyDecorator(
      RESTMethod.GET,
      path,
      target,
      propertyKey,
      descriptor,
      handlers
    );
  };
}

/**
 * Creates a new PUT endpoint on the router.
 * @param {string} path - endpoint path.
 * @param {RequestHandler[]} handlers - request handlers to use.
 */
export function Put(
  path: string = "/",
  ...handlers: RequestHandler[]
): MethodDecorator {
  return function(
    target: any,
    propertyKey: string | symbol,
    descriptor: TypedPropertyDescriptor<any>
  ) {
    applyDecorator(
      RESTMethod.GET,
      path,
      target,
      propertyKey,
      descriptor,
      handlers
    );
  };
}

/**
 * Creates a new PATCH endpoint on the router.
 * @param {string} path - endpoint path.
 * @param {RequestHandler[]} handlers - request handlers to use.
 */
export function Patch(
  path: string = "/",
  ...handlers: RequestHandler[]
): MethodDecorator {
  return function(
    target: any,
    propertyKey: string | symbol,
    descriptor: TypedPropertyDescriptor<any>
  ) {
    applyDecorator(
      RESTMethod.GET,
      path,
      target,
      propertyKey,
      descriptor,
      handlers
    );
  };
}

export function Delete(
  path: string = "/",
  ...handlers: RequestHandler[]
): MethodDecorator {
  return function(
    target: any,
    propertyKey: string | symbol,
    descriptor: TypedPropertyDescriptor<any>
  ) {
    applyDecorator(
      RESTMethod.GET,
      path,
      target,
      propertyKey,
      descriptor,
      handlers
    );
  };
}
