import * as express from "express";

import { Logger } from "@ayana/logger-api";
import { Component, Plugin, PluginAPI } from "@ayanaware/bento";
import { REST_COMPONENT, getRestComponent } from "./decorators";

const log = Logger.get("z");

export interface BentoRESTOptions {
  port?: number;
  app?: express.Express;
}

export class BentoREST implements Plugin {
  public constructor(options: BentoRESTOptions = {}) {
    this.port = options.port || 3000;
    this.app = options.app;
  }

  public readonly port: number;
  public readonly api!: PluginAPI;
  public readonly name: string = "BentoREST";

  public app: express.Express;
  private router: express.Router;
  private readonly routers: Map<string, express.Router> = new Map();

  public async onLoad() {
    log.info("Initializing...");
    if (!this.app) this.app = express();

    this.app.use((req, res, next) => {
      if (this.router) this.router(req, res, next);
      else res.status(503);
    });

    this.app.listen(this.port, () => {
      log.info(`Ready! Serving on "0.0.0.0:${this.port}"`);
    });
  }

  public addRoutes(_router: REST_COMPONENT, context?: any) {
    let router = this.routers.get(_router.name);
    if (!router) {
      router = express.Router();
      this.routers.set(_router.name, router);
      this.rebuildPrimaryRouter();
    }

    for (const route of _router.routes)
      (<any>router)[route.method].apply(router, [
        route.path,
        ...route.handlers,
        async (
          req: express.Request,
          res: express.Response,
          next: express.NextFunction
        ) => {
          try {
            await route.fn.apply(context, [req, res, next]);
          } catch (e) {
            log.error(e);
            res.status(500).json({
              message: "Sorry, I ran into an error."
            });
          }
        }
      ]);
  }

  public removeRoutes(name: string) {
    if (!this.routers.has(name)) return;
    this.routers.delete(name);

    this.rebuildPrimaryRouter();
  }

  public async onPreComponentLoad(component: Component) {
    const _router = getRestComponent(component);
    if (!_router) return;

    this.addRoutes(_router, component);
  }

  public async onPreComponentUnload(component: Component) {
    this.removeRoutes(component.name);
  }

  private rebuildPrimaryRouter() {
    const primaryRouter = express.Router();
    for (const [name, router] of this.routers) primaryRouter.use(name, router);

    this.router = primaryRouter;
  }
}
